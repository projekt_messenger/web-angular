import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild, CanLoad, Router} from '@angular/router';
import {AuthService} from './auth.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad, CanActivateChild {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(): boolean {
    if (this.authService.loggedIn()) {
      return true;
    }
    this.router.navigate(['/']);
  }
  canLoad(): boolean {
    if (this.authService.loggedIn()) {
      return true;
    }
    this.router.navigate(['/']);
  }
  canActivateChild(): boolean {
    return true;
    if (this.authService.loggedIn()) {
      return true;
    }
    this.router.navigate(['/']);
  }
}
