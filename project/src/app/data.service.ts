import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class DataService {
    // result: any;

  //  baseUrl = 'http://localhost:5000/api/';
    baseUrl = 'http://90.178.65.28:8080/api/auth/';
    token: any = localStorage.getItem('token');
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: 'Bearer ' + this.token
        })
    };

  constructor(private http: HttpClient) { }

   getUser() {
      return this.http.get(this.baseUrl + 'user', this.httpOptions);
  }

  getTokenData() {
      const jwt = localStorage.getItem('token');
      // if (localStorage.getItem('token') != null) {
      const jwtData = jwt.split('.')[1];
      const decodedJwtJsonData = window.atob(jwtData);
      const decodedJwtData = JSON.parse(decodedJwtJsonData);
      return decodedJwtData;
  }
    getUserRole() {
        /*const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(localStorage.getItem('token'));
        const role = decodedToken.getRole();*/
        const jwt = localStorage.getItem('token');
        const jwtData = jwt.split('.')[1];
        const decodedJwtJsonData = window.atob(jwtData);
        const decodedJwtData = JSON.parse(decodedJwtJsonData);

        console.log(decodedJwtData);
        console.log(decodedJwtData.role);
        return decodedJwtData.role;
    }
}

