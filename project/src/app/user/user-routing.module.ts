import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {UserComponent} from './user.component';
import {UserProfilComponent} from './user-profil/user-profil.component';
import {UserFriendsComponent} from './user-friends/user-friends.component';
import { ChatMainComponent } from './chat-main/chat-main.component';
import { ChatMainikComponent } from './chat-mainik/chat-mainik.component';


const routes: Routes = [
  {
    path: '',
    component: UserComponent,

    children: [
      {
        path: '',
        children: [
          { path: 'profil', component: UserProfilComponent},
          { path: 'friends', component: UserFriendsComponent},
          { path: 'main', component: ChatMainComponent},
          { path: 'mainik', component: ChatMainikComponent}
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
