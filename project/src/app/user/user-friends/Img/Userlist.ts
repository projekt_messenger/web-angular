export interface Userlist {
    position: string;
    id: number;
    name: string;
    surname: string;
}
