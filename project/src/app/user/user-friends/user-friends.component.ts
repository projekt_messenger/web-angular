import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {AuthService} from '../../auth.service';
import {Router} from '@angular/router';
import {DataService} from '../../data.service';
import {UserService} from '../../user.service';
import { Userlist } from './Img/Userlist';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'app-user-friends',
  templateUrl: './user-friends.component.html',
  styleUrls: ['./user-friends.component.scss']
})
export class UserFriendsComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'surname', 'remove', 'chat'];
 // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    dataSource = new MatTableDataSource<Userlist>(USER_DATA);
  title = 'app';
  users: Userlist[];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor( private router: Router, private service: UserService) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this
        .service
        .getUser()
        .subscribe((data: Userlist[]) => {
          this.users = data;
          console.log(this.users);
        });
    this
        .service
        .getTokenData();
    this
        .service
        .getUserRole();

  }




}


const USER_DATA: Userlist[] = [
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'Varel', surname: 'Varlos'},
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'Pepek', surname: 'Brambora'},
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'Karel', surname: 'Chleba'},
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'Lukáš', surname: 'Monitor'},
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'Jan', surname: 'Novak'},
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'ssteve', surname: 'bbuchanna'},
    {position: 'https://www.naucmese.cz/img/default-user.jpg', id: 1, name: 'Adrián', surname: 'Maskaĺ'}

];
