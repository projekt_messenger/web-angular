import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  showFiller = false;
  constructor() { }

  ngOnInit() {

  }
  delToken() {
    localStorage.clear(); // deleting token from local storage
  }

}



