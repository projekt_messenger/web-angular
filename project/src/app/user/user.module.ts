import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {UserRoutingModule } from './user-routing.module';
import {UserComponent } from './user.component';

import {UserProfilComponent} from './user-profil/user-profil.component';
import {UserFriendsComponent} from './user-friends/user-friends.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ChatMainComponent } from './chat-main/chat-main.component';
import { ChatMainikComponent } from './chat-mainik/chat-mainik.component';

@NgModule({
  declarations: [
      UserComponent,
      UserProfilComponent,
      UserFriendsComponent,
      ChatMainComponent,
      ChatMainikComponent

  ],
  imports: [
    CommonModule,
    UserRoutingModule,
      MatSidenavModule,
      MatTableModule,
      MatPaginatorModule,
      MatFormFieldModule


  ]
})
export class UserModule { }
