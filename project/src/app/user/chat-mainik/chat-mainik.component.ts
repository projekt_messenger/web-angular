import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'app-user-friends',
  templateUrl: './chat-mainik.component.html',
  styleUrls: ['./chat-mainik.component.scss']
})
export class ChatMainikComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  name: string;
  position: number;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Pepa'},
  {position: 2, name: 'Honza'},
  {position: 3, name: 'Pavel'},
  {position: 4, name: 'Karel'},
  {position: 5, name: 'Varel'},
  {position: 6, name: 'Martin'},
  {position: 7, name: 'Marek'},
  {position: 8, name: 'Václav'},
  {position: 9, name: 'Ondřej'},
  {position: 10, name: 'Adrián'},
  {position: 11, name: 'Roman'},
  {position: 12, name: 'Lukáš'},
  {position: 13, name: 'Jakub'},
  {position: 14, name: 'Petr'},
  {position: 15, name: 'person1'},
  {position: 16, name: 'person2'},
  {position: 17, name: 'person3'},
  {position: 18, name: 'person4'},
  {position: 19, name: 'person5'},
  {position: 20, name: 'person6'},
];
