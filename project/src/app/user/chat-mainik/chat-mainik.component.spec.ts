import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatMainikComponent } from './chat-mainik.component';

describe('ChatMainikComponent', () => {
  let component: ChatMainikComponent;
  let fixture: ComponentFixture<ChatMainikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatMainikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatMainikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
