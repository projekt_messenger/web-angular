import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
//  defaultUrl = 'http://localhost:5000/api/auth/';
  defaultUrl = 'http://90.178.65.28:8080/api/auth/';
  constructor(private http: HttpClient) { }
  login(form: any) {
    return this.http.post(this.defaultUrl + 'login', form).pipe(
        map((response: any) => {
          const user = response;
          if (user) {
            localStorage.setItem('token', user.token);
          }
        })
    );
  }
  register(form: any) {
    return this.http.post(this.defaultUrl + 'register', form);
  }
    loggedIn() {
        const token = localStorage.getItem('token');
        return !!token;
    }
}
