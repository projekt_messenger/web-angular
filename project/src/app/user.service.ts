import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {


   url = 'http://90.178.65.28:8080/api/User/';
    token: any = localStorage.getItem('token');
    httpOptions = {
        headers: new HttpHeaders({
            Authorization: 'Bearer ' + this.token
        })
    };

    constructor(private http: HttpClient) { }

    getUser() {
        return this.http.get(this.url , this.httpOptions);
    }
    getUsers() {
        return this
            .http
            .get(`${this.url}` );
    }

    getTokenData() {
        const jwt = localStorage.getItem('token');
        // if (localStorage.getItem('token') != null) {
        const jwtData = jwt.split('.')[1];
        const decodedJwtJsonData = window.atob(jwtData);
        const decodedJwtData = JSON.parse(decodedJwtJsonData);
        return decodedJwtData;
    }

    getUserRole() {
        /*const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(localStorage.getItem('token'));
        const role = decodedToken.getRole();*/
        const jwt = localStorage.getItem('token');
        const jwtData = jwt.split('.')[1];
        const decodedJwtJsonData = window.atob(jwtData);
        const decodedJwtData = JSON.parse(decodedJwtJsonData);

        console.log(decodedJwtData);
        console.log(decodedJwtData.role);
        return decodedJwtData.role;
    }

}
