import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {AuthService} from '../auth.service';
// import { JwtHelperService } from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {DataService} from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public form = {
    email: null,
    password: null
  };
  constructor(private authService: AuthService, private router: Router, private service: DataService) { }
  // private http: HttpClient
  /*onSubmit() {
    console.log(this.form);
    return this.http.post('http://localhost:5000/api/auth/register', this.form).subscribe(
        data => console.log(data),
        error => console.log(error)
    );
  }*/
  login() {
    this.authService.login(this.form).subscribe(data => {
      console.log('success');
    }, error => {
      console.log('failed');
    });
    // routerLink="/user" from html
    if (this.service.getUserRole() === 'user') {
      this.router.navigate(['/user']);
    }
    if (this.service.getUserRole() === 'admin') {
      this.router.navigate(['/admin']);
    }

  }
  ngOnInit() {
    // this.getUserRole();
  }
  loggedIn() {
    const token = localStorage.getItem('token');
    return !!token;
  }
  logout() {
    localStorage.removeItem('token');
  }

}
