import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './login/login.component';
import { CodeComponent } from './code/code.component';
import { UserComponent } from './user/user.component';
// import {ForgottenPasswordComponent} from './forgotten-password/forgotten-password.component';
import {RegisterComponent} from './register/register.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  {path: '' , component: LoginComponent },
  {path: 'code' , component: CodeComponent },
  {path: 'user', loadChildren: './user/user.module#UserModule'},
  {path: 'register', component: RegisterComponent}
//  {path: 'forgottenpassword', component: ForgottenPasswordComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }









