import { Component, OnInit, ViewChild } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public form = {
    email: null,
    password: null,
    name: null,
    surname: null
  };



 constructor(private authService: AuthService) { }
  register() {
    this.authService.register(this.form).subscribe(
        next => console.log('success'), error => console.log(error));
  }
  ngOnInit() {
  }

}









